# abright - the backlight brightness helper tool

The `abright` tool is used to query or set the brightness of the system's
display backlight device.

## Getting the current backlight brightness

The `abright get` command will display the value returned by the `Backlight`
class's `get_percentage()` method: the current brightness value as
a percentage (0 to 100 inclusive) of the maximum one.

## Setting the backlight brightness

The `abright set <value>` command will use the `Backlight` class's
`calc_from_percentage()` method and then write to a sysfs file to set
the current brightness to the specified percentage (0 to 100 inclusive) of
the maximum one.

## Bug^H^H^HPeculiarities

If the system has more than one backlight device, the `abright` tool will
pretty much pick one at random to operate on. Maybe the future will see
a "use this device" configuration setting.

## Contact

The `abright` tool is developed in
[a GitLab repository](https://gitlab.com/ppentchev/abright).
It was written by [Peter Pentchev](mailto:roam@ringlet.net).
