//! Get or adjust the display backlight's brightness.

/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#![deny(missing_docs)]
// Activate most of the clippy::restriction lints...
#![warn(clippy::default_numeric_fallback)]
#![warn(clippy::map_err_ignore)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::pattern_type_mismatch)]
#![warn(clippy::print_stdout)]
#![warn(clippy::unseparated_literal_suffix)]
// ...except for these ones.
#![allow(clippy::implicit_return)]
#![allow(clippy::separated_literal_suffix)]

use std::fs;

use clap::{Parser, Subcommand};
use expect_exit::{self, ExpectedWithError};

use abright::Backlight;

/// The top-level command line parser for the `abright` tool.
#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// The subcommand specifying what to do.
    #[clap(subcommand)]
    command: CliCommands,
}

/// The list of top-level subcommands for the `abright` tool.
#[derive(Subcommand)]
enum CliCommands {
    /// Display the list of features supported by the library and the program.
    #[clap(name = "--features")]
    Features,
    /// Display the current brightness value.
    Get,
    /// Set the current brightness value.
    Set {
        /// The value to set the brightness to as a percentage of the max brightness.
        #[clap(parse(try_from_str=percentage_in_range))]
        value: u32,
    },
}

/// What to do, as parsed from the command-line options.
#[derive(Debug)]
enum Mode {
    /// Display the list of features supported by the library and the program.
    Features,
    /// Display the current brightness value.
    Get,
    /// Set the current brightness value.
    Set(u32),
}

/// Validate an unsigned percentage value (0 to 100 inclusive).
fn percentage_in_range(value: &str) -> Result<u32, String> {
    #[allow(clippy::map_err_ignore)]
    let res: u32 = value
        .parse()
        .map_err(|_| "Not an unsigned integer percentage value".to_owned())?;
    if res <= 100 {
        Ok(res)
    } else {
        Err("Invalid percentage value, must be at most 100".to_owned())
    }
}

/// Parse the command-line arguments.
fn parse_args() -> Mode {
    let cli = Cli::parse();
    match cli.command {
        CliCommands::Features => Mode::Features,
        CliCommands::Get => Mode::Get,
        CliCommands::Set { value } => Mode::Set(value),
    }
}

/// Display the current brightness as a percentage of the total.
#[allow(clippy::print_stdout)]
fn cmd_get() {
    let blight = Backlight::find_backlight().or_exit_e_("Could not find a backlight device");
    let res = blight
        .get_percentage()
        .or_exit_e_("Could not determine the current brightness percentage");
    println!("{}", res);
}

/// Set the current brightness value as a percentage of the max brightness.
fn cmd_set(value: u32) {
    let blight = Backlight::find_backlight().or_exit_e_("Could not find a backlight device");
    let res = blight
        .calc_from_percentage(value)
        .or_exit_e_("Could not calculate the new brightness value");
    fs::write(blight.brightness(), &format!("{}\n", res)).or_exit_e(|| {
        format!(
            "Could not write '{}' to the {} brightness file",
            res,
            blight.brightness().display()
        )
    });
}

/// Display the list of features supported by the library and the program.
#[allow(clippy::print_stdout)]
fn cmd_features() {
    let features: Vec<_> = abright::FEATURES
        .iter()
        .map(|&(name, version)| format!("{}={}", name, version))
        .collect();
    println!("Features: {}", features.join(" "));
}

fn main() {
    match parse_args() {
        Mode::Features => cmd_features(),
        Mode::Get => cmd_get(),
        Mode::Set(value) => cmd_set(value),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_features() {
        super::cmd_features();
    }

    #[test]
    fn test_get() {
        super::cmd_get();
    }
}
