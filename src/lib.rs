//! A helper library for getting and setting a backlight device's properties.

/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#![deny(missing_docs)]
// Activate most of the clippy::restriction lints...
#![warn(clippy::default_numeric_fallback)]
#![warn(clippy::map_err_ignore)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::pattern_type_mismatch)]
#![warn(clippy::print_stdout)]
#![warn(clippy::unseparated_literal_suffix)]
// ...except for these ones.
#![allow(clippy::implicit_return)]
#![allow(clippy::separated_literal_suffix)]

use std::fs;
use std::path::{Path, PathBuf};

use quick_error::quick_error;

quick_error! {
    /// An error that occurred during the handling of the backlight devices.
    #[derive(Debug)]
    #[non_exhaustive]
    pub enum BacklightError {
        /// Could not read a file's contents.
        FileRead(tag: String, path: String, err: String) {
            display("Could not read the {} value from {}: {}", tag, path, err)
        }
        /// Could not parse a file's contents.
        FileParse(tag: String, path: String, val: String, err: String) {
            display(
                "Could not parse the value {:?} read from the {} file {} as an unsigned integer: {}",
                val, tag, path, err
            )
        }
        /// Could not scan the top-level backlight devices directory.
        DirScan(path: String, err: String) {
            display("Could not scan the {} directory: {}", path, err)
        }
        /// No backlight device found.
        NoDevice(topdir: String) {
            display("Could not find a valid backlight subdir in the {} directory", topdir)
        }
        /// Something went unexpectedly wrong during the calculations.
        InternalError(tag: String) {
            display("Internal error: {}", tag)
        }
    }
}

/// The default sysfs path to the list of backlight devices.
const PATH_CLASS_BACKLIGHT: &str = "/sys/class/backlight";

/// The list of features supported by the library.
pub const FEATURES: [(&str, &str); 1] = [("abright", env!("CARGO_PKG_VERSION"))];

/// A backlight device as represented by sysfs.
#[derive(Debug)]
#[non_exhaustive]
pub struct Backlight {
    /// The path to the file containing the current value for the brightness.
    brightness: PathBuf,
    /// The path to the file containing the maximum value for the brightness.
    max_brightness: PathBuf,
}

impl Backlight {
    /// Get the path to the file containing the current value.
    #[inline]
    #[must_use]
    pub const fn brightness(&self) -> &PathBuf {
        &self.brightness
    }

    /// Get the path to the file containing the maximum value.
    #[inline]
    #[must_use]
    pub const fn max_brightness(&self) -> &PathBuf {
        &self.max_brightness
    }

    /// Read a file, parse it as an unsigned integer.
    #[inline]
    fn get_value<P: AsRef<Path>>(path: P, tag: &str) -> Result<u32, BacklightError> {
        let path_str = || format!("{}", path.as_ref().display());
        let val = fs::read_to_string(path.as_ref())
            .map_err(|err| BacklightError::FileRead(tag.to_owned(), path_str(), err.to_string()))?;
        val.trim_end().parse::<u32>().map_err(|err| {
            BacklightError::FileParse(tag.to_owned(), path_str(), val, err.to_string())
        })
    }

    /// Get the numerical value for the backlight's current brightness.
    ///
    /// # Errors
    /// - filesystem errors while reading the file
    /// - conversion errors while parsing the file's contents as an unsigned integer
    #[inline]
    pub fn get_brightness(&self) -> Result<u32, BacklightError> {
        Self::get_value(&self.brightness, "brightness")
    }

    /// Get the numerical value for the backlight's maximum brightness.
    ///
    /// # Errors
    /// - filesystem errors while reading the file
    /// - conversion errors while parsing the file's contents as an unsigned integer
    #[inline]
    pub fn get_max_brightness(&self) -> Result<u32, BacklightError> {
        Self::get_value(&self.max_brightness, "max_brightness")
    }

    /// Find a backlight device that seems to be operational.
    ///
    /// # Errors
    /// - filesystem errors while scanning the backlight devices sysfs tree
    /// - [`BacklightError::NoDevice`] if no backlight device was found
    #[inline]
    pub fn find_backlight() -> Result<Self, BacklightError> {
        let topdir = Path::new(PATH_CLASS_BACKLIGHT);
        let top_str = || format!("{}", topdir.display());
        for cand_res in topdir
            .read_dir()
            .map_err(|err| BacklightError::DirScan(top_str(), err.to_string()))?
        {
            let cand =
                cand_res.map_err(|err| BacklightError::DirScan(top_str(), err.to_string()))?;
            let brightness = cand.path().join("brightness");
            let max_brightness = cand.path().join("max_brightness");
            if brightness.is_file() && max_brightness.is_file() {
                return Ok(Self {
                    brightness,
                    max_brightness,
                });
            }
        }
        Err(BacklightError::NoDevice(format!("{}", topdir.display())))
    }

    /// Get the current brightness as a percentage of the maximum one.
    ///
    /// # Errors
    /// - any errors returned by [`Self::get_brightness()`] or [`Self::get_max_brightness()`]
    /// - multiplication overflow, division by zero
    #[inline]
    pub fn get_percentage(&self) -> Result<u32, BacklightError> {
        let brightness = self.get_brightness()?;
        let max_brightness = self.get_max_brightness()?;
        brightness
            .checked_mul(100)
            .ok_or_else(|| BacklightError::InternalError("brightness value too large".to_owned()))?
            .checked_div(max_brightness)
            .ok_or_else(|| BacklightError::InternalError("zero max_brightness".to_owned()))
    }

    /// Calculate the new value to write to the brightness file as
    /// the specified percentage of the maximum one.
    ///
    /// # Errors
    /// - any errors returned by [`Self::get_max_brightness()`]
    /// - (unexpected) multiplication overflow, division by zero
    #[inline]
    pub fn calc_from_percentage(&self, value: u32) -> Result<u32, BacklightError> {
        let max_brightness = self.get_max_brightness()?;
        value
            .checked_mul(max_brightness)
            .ok_or_else(|| {
                BacklightError::InternalError("value unexpectedly too large".to_owned())
            })?
            .checked_div(100)
            .ok_or_else(|| BacklightError::InternalError("could not divide by 100".to_owned()))
    }
}

#[cfg(test)]
mod tests {
    use std::error::Error;

    use super::Backlight;

    #[test]
    fn test_find() -> Result<(), Box<dyn Error>> {
        let blight = Backlight::find_backlight()?;
        assert!(format!("{}", blight.brightness.display()).starts_with(super::PATH_CLASS_BACKLIGHT));
        assert!(
            format!("{}", blight.max_brightness.display()).starts_with(super::PATH_CLASS_BACKLIGHT)
        );
        Ok(())
    }

    #[test]
    fn test_get() -> Result<(), Box<dyn Error>> {
        let value = Backlight::find_backlight()?.get_percentage()?;
        assert!(value <= 100);
        Ok(())
    }

    #[test]
    fn test_calc_from() -> Result<(), Box<dyn Error>> {
        let blight = Backlight::find_backlight()?;
        let max = blight.get_max_brightness()?;
        assert_eq!(0, blight.calc_from_percentage(0)?);
        assert_eq!(max, blight.calc_from_percentage(100)?);
        assert!(blight.calc_from_percentage(13)? > 0);
        assert!(blight.calc_from_percentage(13)? < max);
        Ok(())
    }

    #[test]
    fn test_features() {
        assert!(super::FEATURES.iter().any(|&(name, _)| name == "abright"));
    }
}
